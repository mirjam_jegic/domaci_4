<head><link rel="stylesheet" href="style.css"></head>

<?php

$proizvodi = [
  "coca cola" => 130,
  "kikiriki" => 50,
  "smoki" => 70
];


function dodajProizvod($proizvodi, $naziv, $cena){

  if (array_key_exists($naziv,$proizvodi))
  {
    $ispis['poruka']= "Proizvod $naziv već postoji u tabeli!";
  }
  else
  {
    $proizvodi[$naziv]=$cena;
    $ispis['poruka']="Dodali ste nov proizvod: $naziv";
  }

  $ispis['isp']=$proizvodi;
  return $ispis;
}


$nov = dodajProizvod($proizvodi, 'cips', 90);
$proizvodi= $nov['isp'];
echo $nov['poruka'];
?>

<table class="frame">

  <tr class="header">
    <th>Naziv</th>
    <th>Cena</th>
  </tr>


  <?php
  foreach ($proizvodi as $naziv => $cena) {      
   echo '<tr>';
   echo '<td>' . $naziv . '</td>';
   echo '<td class="right">' . number_format($cena,2,",",".") . '</td>';
   }
  ?>
  
</table>

